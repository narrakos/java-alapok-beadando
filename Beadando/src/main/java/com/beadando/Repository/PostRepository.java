package com.beadando.Repository;

import com.beadando.Entity.Blogpost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Transactional
@Repository
public interface PostRepository extends JpaRepository<Blogpost, Long> {


}
