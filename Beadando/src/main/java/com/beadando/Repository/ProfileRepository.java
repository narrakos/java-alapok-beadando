package com.beadando.Repository;

import com.beadando.Entity.Profile;
import org.hibernate.QueryException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.function.LongToIntFunction;

@Transactional
@Repository
public interface ProfileRepository  extends JpaRepository<Profile, Long> {

    @Query("SELECT n FROM Profile n where n.userName= :username")
    Profile findByName(@Param("username") String username);


}
