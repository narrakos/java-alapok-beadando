package com.beadando.Controller;


import com.beadando.Entity.Profile;
import com.beadando.Repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

@Controller
public class ProfileController {

    @Autowired
    private ProfileRepository repository;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String LoginView(){
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            HttpSession session,
            Model model
    ){
        Profile profile = repository.findByName(username);
        if (profile != null && profile.getUserPassword().equals(password) ){
            model.addAttribute("loginMsg", "Successful login");
            session.setAttribute("username", username);
        } else {
            model.addAttribute("loginMsg", "Login failed");
        }

    }

    @RequestMapping(value = "/logout")
    public String logout( HttpSession session){
        session.removeAttribute("username");
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerView(){
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "password2") String password2,
            Model model){
        if (!password.equals(password2)){
           model.addAttribute("registerMsg", "Passwords not matching");
            return;
        } else if (repository.findByName(username) != null){
            model.addAttribute("registerMsg", "Username taken");
            return;
        } else {
            repository.save(new Profile(username, password));
            model.addAttribute("registerMsg", "Successful registration");
        }
    }

}
