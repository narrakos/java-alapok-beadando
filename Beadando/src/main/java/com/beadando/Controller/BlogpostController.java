package com.beadando.Controller;

import com.beadando.Entity.Blogpost;
import com.beadando.Entity.Profile;
import com.beadando.Repository.PostRepository;
import com.beadando.Repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class BlogpostController {

    @Autowired
    private PostRepository repository;
    @Autowired
    private ProfileRepository profileRepository;

    @RequestMapping(value = "/blogpost", method = RequestMethod.GET)
    public String blogPostView(){
        return "blogPost";
    }

    @RequestMapping(value = "/blogpost", method = RequestMethod.POST)
    public void blogPost(
            @RequestParam("title") String title,
            @RequestParam("blogPostBody") String body,
            HttpSession session
    ){
        repository.save(new Blogpost(profileRepository.findByName(session.getAttribute("username").toString()),
                title, body));
    }
}
