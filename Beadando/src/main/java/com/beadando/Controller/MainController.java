package com.beadando.Controller;

import com.beadando.Entity.Blogpost;
import com.beadando.Repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class MainController {

    @Autowired
    private PostRepository repository;

    @RequestMapping(value = "/")
    public String index(Model model){
        List<Blogpost> blogposts = repository.findAll(new Sort(Sort.Direction.DESC, "creationDate"));
        model.addAttribute("posts", blogposts);
        return "index";
    }
}
